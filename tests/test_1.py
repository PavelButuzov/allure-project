import allure
import pytest


class Test1:
    def setup_method(self):
        pass

    def teardown_method(self):
        pass

    @allure.title("Поиск города без значений")
    @allure.label('testcase', '1141311')
    def test_2(self):
        with allure.step('Тест 1'):
            assert True
            with allure.step('Тест 1.1'):
                assert True
                with allure.step('step 1.1.1'):
                    allure.attach('t1.txt', attachment_type=allure.attachment_type.TEXT, name='tratata1')
                    assert True
            with allure.step('step 1.2'):
                assert True
        with allure.step('step 2'):
            assert True

    @allure.title('Test 1')
    @allure.description('desc 1')
    @allure.link(url='https://www.google.com/', name='GOOGLE')
    @allure.tag('123', '321')
    @pytest.mark.parametrize('var1, var2, var3', [(122, 222, 333), (333, 222, 4211), (0000, 0000, 0000)])
    def test_1(self, var1, var2, var3):
        allure.dynamic.label('testcase', '1141312')
        #allure.label('testcase', '1141312')
        with allure.step('step 1'):
            assert True
            with allure.step('step 1.1'):
                assert True
                with allure.step('step 1.1.1'):
                    allure.attach('t2.txt', attachment_type=allure.attachment_type.TEXT, name='tratata2')
                    assert True
            with allure.step('step 1.2'):
                assert True
        with allure.step('step 2'):
            assert True


@allure.title('Тест 6 - (STRING)')
@allure.description('desc 3')
@allure.tag('132', '123')
@pytest.mark.skip
def test_3():
    allure.dynamic.label('testcase', '1163079')
    with allure.step('step 1'):
        assert True
        with allure.step('step 1.1'):
            assert True
            with allure.step('step 1.1.1'):
                allure.attach('t3.txt', attachment_type=allure.attachment_type.TEXT)
                assert True
        with allure.step('step 1.2'):
            assert True
    with allure.step('step 2'):
        assert True


class Test2:
    @allure.title('Test 33')
    @allure.description('desc 4')
    def test_4(self):
        with allure.step('step 1'):
            assert True
            with allure.step('step 1.1'):
                assert True
                with allure.step('step 1.1.1'):
                    allure.attach('t4.txt', attachment_type=allure.attachment_type.TEXT)
                    assert True
            with allure.step('step 1.2'):
                assert True
        load_im(1, 2)
        with allure.step('step 2'):
            assert True


@allure.step
def load_im(a, b):
    pass
